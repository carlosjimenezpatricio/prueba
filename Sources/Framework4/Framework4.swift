import Foundation
import Alamofire

public class Utils {
    public init() { }

    public func version() -> String {
        return "0.1.0"
    }

    public func request() {
        _ = try? AF.request("https://www.elmundo.es".asURL())
    }
}
