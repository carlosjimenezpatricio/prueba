import XCTest

import Framework4Tests

var tests = [XCTestCaseEntry]()
tests += Framework4Tests.allTests()
XCTMain(tests)
