// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Framework4",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "Framework4 Dynamic Library",
            targets: ["Framework4 Dynamic Library"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(name: "Alamofire", url: "https://github.com/alamofire/alamofire.git", from: "5.4.1"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "Framework4",
            exclude: ["Package.swift"]
        ),
        .binaryTarget(
            name: "Framework4 Dynamic Library",
            path: "./Framework.xcframework")
    ]
)
